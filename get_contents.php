<?php 

$content = $_POST['c'];
$result = "";
$anweredQuestions = "";
$answeredColors = "";
$q = "";
$p = "";
$answered = array();
$answeredColor = array();

if (isset($_POST['answered']))
{
    $anweredQuestions = $_POST['answered'];
}

if (isset($_POST['answeredColors']))
{
    $answeredColors = $_POST['answeredColors'];
}

if ($content == "example")
{
    $result = file_get_contents(__DIR__ . "/example.html");
}
else if ($content == "finish")
{
    $result = file_get_contents(__DIR__ . "/finish.html");
}
else 
{
    $parts = explode(",", $anweredQuestions);
	$partsColors = explode(",", $answeredColors);

    foreach ($parts as $part)
    {
        $answered[trim($part)] = 1;
    }
	
	foreach ($partsColors as $part)
    {
        $answeredColor[trim($part)] = 1;
    }

    while (true)
    {
        $q = rand(1, 4);
        if (!isset($answered[$q]))
        {
            if ($anweredQuestions == "")
            {
                $anweredQuestions .= "$q";
            }
            else 
            {
                $anweredQuestions .= ",$q";
            }
            
            break;
        }
    }
	
	while (true)
    {
        $p = rand(1, 4);
        if (!isset($answeredColor[$p]))
        {
            if ($answeredColors == "")
            {
                $answeredColors .= "$p";
            }
            else 
            {
                $answeredColors .= ",$p";
            }
            
            break;
        }
    }

    if ($q == 1)
    {
		$result = file_get_contents(__DIR__ . "/text1.html");
    }
    else if ($q == 2)
    {
		$result = file_get_contents(__DIR__ . "/text2.html");
    }
    else if ($q == 3)
    {
		$result = file_get_contents(__DIR__ . "/text3.html");
    }
    else if ($q == 4)
    {
		$result = file_get_contents(__DIR__ . "/text4.html");
    }
 
}

$res = array();
$res['result'] = $result;
$res['meta'] = $anweredQuestions;
$res['metaColor'] = $answeredColors;
$res['q'] = $q;
$res['p'] = $p;
$res['last'] = 0;

if(count($answered) == 3)
{
    $res['last'] = 1;
}

echo json_encode($res);// $result;
