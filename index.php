<!doctype html>
<html style="height: 100%;">
<head>
<meta charset="utf-8">
<title>Teksta lasāmības anketa</title>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<style>
    html, body 
    {margin: 0; height: 100%; }
	
	.black_white
	{
		background-color: black;
		color: white;
	}
	
	.white_black
	{
		background-color: white;
		color: black;
	}
	
	.white_red
	{
		background-color: white;
		color: red;
	}
	
	.black_red
	{
		background-color: black;
		color: red;
	}
	
	#textContainer
	{
	    padding: 2%;
	}
	
</style>
</head>
<body style="height: 100%;">
<div style="height: 100%" class="container">
<div style="margin-top: 10px;" class="row">

    <h1>Čav! <br/> Šajā vietnē ir aptauja priekš mana maģistrantūras kursa pētījuma. <br/><br/>
    </h1>

    <h3>Aptaujā jāatbild uz 4 jautājumiem, kur katrā būs jāizvēlas pareizā atbilde pēc teksta fragmenta izlasīšanas.</h3>
    <h3>Nākamajā sadaļā būs viens parauga jautājums, lai saprastu aptaujas ideju.</h3>
    <h3>Centies atbildēt uz jautājumiem pēc iespējas ātrāk :)</h3>

</div>
<div class="row"></div>
<div class="row ">
    <button class="btn btn-primary" id="nextExampleBtn">Tālāk</button>
</div>





</div>
<script type="text/javascript" src="main.js"></script>
</body>
</html>
