
var answered = "";
var answeredColors = "";
var poll_results = {};
var question = "";
var questionVariant = "";
var isLast = 0;

var startTime = new Date();
var endTime;


function submitPoll()
{
	console.log("clicked");

	$.ajax({
		type: "POST",
		url: "save_poll.php",
		data: {data: JSON.stringify(poll_results)},
		success: function(resp)
		{
			$('.container').html("<h1>Paldies!</h1>");

		},
	  });

}

$(document).on("click", "#showQuestion", function()
{
	console.log("clicked");
	$('#textContainer').hide();
	$('#question').show();
	$(this).hide();
	$('#nextQuestionBtn').show();

});

$(document).on("click", "#finishBtn", function()
{
	poll_results['dzimums'] = $("input[name='dzimums']:checked").val();
	poll_results['vecums'] = $('#age').val();

	submitPoll();
});

$(document).on("click", "#nextQuestionBtn", function()
{
	var selectedOption = $("input[name='exampleRadios']:checked").val();
	var correctOption = $('#nextQuestionBtn').attr("data-c");
	var isCorrect = (selectedOption == correctOption);
	var cont = "";

	endTime = new Date();
	var timeDiff = endTime - startTime;
	// strip the ms
	timeDiff /= 1000;
	var seconds = Math.round(timeDiff);

	if (question != "")
	{
		poll_results[question] = {correct: isCorrect,
								selectedOption: selectedOption,
								correctOption: correctOption,
								seconds: seconds,
								variation: questionVariant};
	}

	if (isLast == 1)
	{
		cont = "finish";
		//return;
	}

	$.ajax({
		type: "POST",
		url: "get_contents.php",
		data: {'answered': answered,
				'answeredColors': answeredColors,
				c: cont,
				p: questionVariant},
		success: function(resp)
		{
			var res = JSON.parse(resp);
			$('.container').html(res.result);
			answered = res.meta;
			answeredColors = res.metaColor;
			question = res.q;
			questionVariant = res.p;
			isLast = res.last;
			
			if (questionVariant == 1)
			{
				$('#textContainer').addClass("black_white");
			}
			else if (questionVariant == 2)
			{
				$('#textContainer').addClass("white_black");
			}
			else if (questionVariant == 3)
			{
				$('#textContainer').addClass("white_red");
			}
			else if (questionVariant == 4)
			{
				$('#textContainer').addClass("black_red");
			}

			startTime = new Date();
			//console.log("resp", resp);
		},
	  });
});


$("#nextExampleBtn").click(function()
{
	console.log("get example");

	$.ajax({
		type: "POST",
		url: "get_contents.php",
		data: {c: "example"},
		success: function(resp)
		{
			var res = JSON.parse(resp);
			$('.container').html(res.result);
			answered = res.meta;
			console.log("resp", resp);
		},
	  });



});